#!/usr/bin/python3
import sys
import os
import argparse
import pandas as pd


###
# This script reads in results files of MLVA_finder and compares and then ranks these results between the sample and a "database" collection.
# Input 1: MLVA results from sample
# Input 2: MLVA results from database collection
# Output: Ranked list of database entrys with indicator column containing the number of matching MLVA values
###

def parse_args():
    parser = argparse.ArgumentParser(description='Compare MLVA_finder results')
    parser.add_argument('--sample-in', dest='sample_in', required=True, help='Sample MLVA_finder results CSV')
    parser.add_argument('--database-in', dest='database_in', required=True, help='Database MLVA_finder results CSV')
    parser.add_argument('--ranked-db-out', dest='db_out', required=True, help='Ranked database MLVA_finder results')
    return parser.parse_args()

def read_sample(sample_csv):
    try:
        sample_in = pd.read_csv(sample_csv)
        return sample_in
    except IOError:
        sys.exit(f'ERROR: File {sample_csv} not found.')

def read_database(database_csv):
    try:
        database_in = pd.read_csv(database_csv)
        return database_in
    except IOError:
        sys.exit(f'ERROR: File {database_csv} not found.')

if __name__ == '__main__':
    args = parse_args()

    sample_in = read_sample(args.sample_in)
    assert len(sample_in) == 1, f'sample_in may only contain one sample'
    database_in = read_database(args.database_in)
    assert len(database_in) > 0, f'database_in needs to contain at least one entry'
    assert sample_in.columns.all() == database_in.columns.all(), f'Column names of sample and database do not match'

    # Get Access number/name of sample that is compared to
    sample_name = sample_in['Access_number'][0]
    

    # sample_in duplicated/inflated by the size of database_in : pd.concat([sample_in]*len(database_in)
    # compare both dataframes with pandas.eq, return df w/ True for matching entries
    # sum up the True entries of every row w/ .sum(1) (1 indicates columns)
    # store these values in a new column in database_in that is named after the sample_name
    database_in[sample_name] = database_in.eq(pd.concat([sample_in]*len(database_in), ignore_index=True)).sum(1)

    # sort database_in by new ranking column
    database_out = database_in.sort_values(sample_name, ascending=False, ignore_index=True)

    # make the new database output pretty (skip key column, replace NaN by blank space)
    database_out.drop('key', axis=1, inplace=True)
    database_out.fillna('', inplace=True)
    database_out.to_csv(args.db_out, index=False)