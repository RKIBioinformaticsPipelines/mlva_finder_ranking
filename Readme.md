# mlva_finder_ranking

A simple Python script to rank result files from [MLVA_finder](https://github.com/i2bc/MLVA_finder)

The ranking script simply compares the entries of each column of the MLVA results file of 1. a sample sequence to 2. self chosen reference sequences. It counts  exact accordances of the allele frequency values. These counts are stored in a new column named after the sample-in input and the table is sorted according to that column. So the best fitting database sequence should be on top.

## Dependencies

```
conda create -n mlva_env -c conda-forge -c anaconda -c bioconda python pandas python-devtools regex
```
This environment includes the dependencies from MLVA_finder and can also be used to run MLVA_finder.

## How-To

* clone MLVA_finder and mlva_finder_ranking
* run MLVA_finder on a single sample, e.g. 
```
python3 MLVA_finder.py --input my_assembly/ --output my_assembly_results/ --primer MLVA_finder/data_test/primers/Brucella_primers.txt -m 2 --predicted-PCR-size-table --mixte - v
```
* run MLVA_finder on a database collection, e.g. from the test data set
```
python3 MLVA_finder.py --input MLVA_finder/data_test/assemblies/Brucella/ --output database_results/ --primer MLVA_finder/data_test/primers/Brucella_primers.txt -m 2 --predicted-PCR-size-table --mixte - v
```
* run mlva_finder_ranking on the MLVA_analysis_*.csv result files
```
python3 mlva_ranking.py --sample-in my_assembly_results/MLVA_analysis_my_assembly.csv --database-in database_results/MLVA_analysis_Brucella.csv --ranked-db-out my_assembly_results/ranked_MLVA_analysis_my_assembly.csv
```


## Example

Taken the example Brucella test data set from [MLVA_finder](https://github.com/i2bc/MLVA_finder), the results file `ranked_MLVA_analysis_my_assembly.csv`would look like this.

We took the database sequence `Brucella_melitensis_NZ_CP008750.1_NZ_CP008751.1.fa` as a sample file and copied it to `my_assembly/my_assembled_sequence.fa`.
The best hit `Brucella_melitensis_NZ_CP008750` has 78 matching Bruce* entries.


| Access_number                       | Bruce06-1322 | Bruce08-1134 | Bruce11-211 | Bruce12-73 | ... | Bruce78-181 | Bruce79-163 | Bruce80-542 | my_assembled_sequence |
|-------------------------------------|--------------|--------------|-------------|------------|-----|-------------|-------------|-------------|-----------------------|
| Brucella_melitensis_NZ_CP008750     | 1            | 5            | 3.0         | 13.0       |     | 2.0         | 4.0         | 6           | 78                    |
| Brucella_melitensis_M28_NC_017244   | 1            | 5            | 3.0         | 13.0       |     | 2.0         | 4.0         | 5           | 66                    |
| Brucella_melitensis_M5-90_NC_017246 | 1            | 5            | 3.0         | 13.0       |     | 2.0         | 4.0         | 5           | 65                    |
| Brucella_melitensis_NI_NC_017248    | 1            | 5            | 3.0         | 13.0       |     | 2.0         | 4.0         | 4           | 64                    |
